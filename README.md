Number of orders. Version 1.0

Module for CS-Cart. On the customer list page, add a column with the number of orders for each buyer. And also adds sorting by the number of orders.

For installing the module extract to the root directory CS-Cart.